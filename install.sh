#!/bin/bash

(exec mkdir /root/Documents/sagitarius/logs)
(exec mkdir /root/Documents/sagitarius/downloaded_files)

(exec touch ~/.bash_aliases)
(exec echo 'alias sagitarius="/root/Documents/sagitarius/scripts/exec.sh"' >> ~/.bash_aliases)
(source ~/.bash_aliases)