#!/bin/bash

while getopts i: option
do
	case "${option}"
		in
		i) IP=${OPTARG};;
	esac
done

HOSTS=/root/Documents/sagitarius/logs/hosts.txt
VICTIMS=/root/Documents/sagitarius/logs/victims.txt
LOGS=/root/Documents/sagitarius/logs

echo "Gathering all UP hosts..."

(exec nmap -sP "$IP" > "$HOSTS")
(cat "$HOSTS" | grep "Nmap scan report for" | tr -d "Nmap scan report for" > "$HOSTS") 

echo "Hosts registered"
echo ""
echo "Searching for OS informations and vulnerabilities..."

(exec rm -f "$VICTIMS")
(exec touch "$VICTIMS")

while read host; do
	(exec touch "$LOGS"/"$host".txt)
	(echo "#######################################LOG START#######################################" > "$LOGS"/"$host".txt)
	(echo "" >> "$LOGS"/"$host".txt)
	(echo "$-----------------------------$host scan : OS informations-----------------------------" >> "$LOGS"/"$host".txt)
	(echo "" >> "$LOGS"/"$host".txt)
	(exec nmap -O "$host" >> "$LOGS"/"$host".txt)
	(echo "" >> "$LOGS"/"$host".txt)
	(echo "-----------------------------$host scan : Vulnerability scan--------------------------" >> "$LOGS"/"$host".txt)
	(echo "" >> "$LOGS"/"$host".txt)
	(exec nmap -v --script vuln "$host" >> "$LOGS"/"$host".txt)
	(echo "" >> "$LOGS"/"$host".txt)
	(echo "#######################################LOG END#######################################" >> "$LOGS"/"$host".txt)
	if (cat "$LOGS"/"$host".txt | grep -i "vulnerable" | grep -v -i "Hosts are all up")
	then
		echo "$host : vulnerable"
		echo "$host" >> "$VICTIMS"
	fi
done < "$HOSTS"

echo "OS and vulnerability found"
echo ""

