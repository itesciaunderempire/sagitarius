#!/bin/bash

while getopts i:e: option
do
	case "${option}"
		in
		i) IPRANGE=${OPTARG};;
		e) EXPLOIT=${OPTARG};;
	esac
done
(exec clear)
sleep 1
echo '##################################################################'
echo '#                                 __                             #'                                                     
echo '#                                / _\ #                          #'
echo '#                                \c /  #                         #'
echo '#                                / \___ #                        #'
echo '#                                \`----`#==>                     #'
echo '#                                |  \  #                         #'
echo '#                     ,%.-"""---"`--"\#_                         #'
echo '#                    %%/             |__`\                       #'
echo '#                   .%"\     |   \   /  //                       #'
echo '#                   ,%" >   ."----\ |  [/                        #'
echo '#                      < <<`       ||                            #'
echo '#                       `\\\       ||                            #'
echo '#                         )\\      )\                            #'
echo '#                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                   #'
echo '#                                                                #'
echo '#            ____          _ __  __           _                  #'
echo '#           / __/__ ____ _(_) /_/ /____ _____(_)_ _____          #'
echo '#          _\ \/ _ `/ _ `/ / __/ __/ _ `/ __/ / // (_-<          #'
echo '#         /___/\_,_/\_, /_/\__/\__/\_,_/_/ /_/\_,_/___/          #'
echo '#                  /___/                                         #'
echo '#                                                                #'
echo "##################################################################"
echo ""
echo ""
SCANNING_SCRIPT=/root/Documents/sagitarius/scripts/scanning.sh
EXPLOIT_SCRIPT=/root/Documents/sagitarius/scripts/exploit.sh
LOGS_DIR=/root/Documents/sagitarius/logs
DOWNLOADS_DIR=/root/Documents/sagitarius/downloaded_files
sleep 3
echo ""
echo ""
echo "=============================SCANNING============================="
echo ""
echo ""
(exec "$SCANNING_SCRIPT" -i "$IPRANGE")
echo ""
echo ""
sleep 3
echo "=============================EXPLOIT=============================="
echo ""
echo ""
(exec "$EXPLOIT_SCRIPT" -e "$EXPLOIT")
echo ""
echo ""
sleep 3
echo "==============================LOGS================================"
echo ""
echo ""
(exec ls "$LOGS_DIR")
echo ""
echo ""
echo "========================DOWNLOADED=FILES=========================="
echo ""
echo ""
(exec ls "$DOWNLOADS_DIR")
echo ""
echo "" 
echo "##################################################################"
echo "#                                                                #"
echo "#                         TEST COMPLETE                          #"
echo "#                                                                #"
echo "##################################################################"